# Rancid 

###### Dylan Hamel - HEIG-VD - Décembre 2018

![rancid-schema.png](./rancid-schema.png)

## Installation

Installation de Rancid + Gitlab.
Gitlab va contenir les backups des devices réseaux (Cisco.)
Le réseau est simulé sur GNS3

![network](./network.png)

### Rancid

* CentOS

Pré-requis - mieux avant l'installation

```bash
su root
yum update
yum upgrade
yum install -y cvs vim wget
```

Créer un utlilisateur pour ```rancid```

```bash
groupadd netadm
useradd -g netadm -c "Networking Backups" -d /home/rancid rancid
```

Installation de Rancid

```bash
# su root 
su rancid

cd /home/rancid/
mkdir TAR
cd TAR
wget ftp://ftp.shrubbery.net/pub/rancid/rancid-3.7.tar.gz
tar -zxvf rancid-3.7.tar.gz
cd rancid-3.7
./configure --prefix=/usr/local/rancid
make install

# Remove file
cd /home/rancid/
rm -r rancid-3.7.tar.gz
```

Créer une fichier ```.cloginrc``` dans le dossier ```/home/rancid/``` de l'utilisateur ```rancid```

``````bash
cd /home/rancid/TAR/rancid-3.7 
cp cloginrc.sample /home/rancid/.cloginrc
chmod 0600 /home/rancid/.cloginrc
chown -R rancid:netadm /home/rancid/.cloginrc
chown -R rancid:netadm /usr/local/rancid/
chmod 775 /usr/local/rancid/
``````

Créer maintenant les groupes dans lesquels seront mis les différents équipements.

* cisco-rt => contient les routeurs
* cisco-sw => contient les switches

éditer le fichier ```/usr/local/rancid/etc/rancid.conf``` (ligne 106 ~)

```bash
# list of rancid groups
LIST_OF_GROUPS="cisco-rt cisco-sw"
```

Rancid permet d'envoyer des emails pour montrer la différence entre les fichiers. Ajouter des nouveaux aliases dans ```/etc/aliases```

```bash
# à la ligne ~83 commenter noc:
# noc:		root

#
# Rancid email addresses
#
rancid-admin-networking:        rancid-networking
rancid-networking:              noc
noc:                            dylan.hamel@heig-vd.ch
```

```bash
[root@centos etc]# newaliases
```

Utiliser l'utilisateur ```rancid``` pour lancer la commande ```rancid-cvs```

```bash
su rancid
id
uid=1001(rancid) gid=1001(netadm) groupes=1001(netadm) contexte=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
```

=> Possibilité de désactiver ```SELINUX```

```bash
[rancid@centos etc]$ sestatus
SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   enforcing
Mode from config file:          enforcing
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Max kernel policy version:      28

# Passer en root
[root@centos etc]# vim /etc/selinux/config
```

```bash
# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=disabled
# SELINUXTYPE= can take one of three two values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected. 
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted 

```

=> Need to reboot your CentOS

Pour désactiver temporairement ```SELINUX```

```bash
sudo setenforce 0
```

Avec l'utilisateur ```rancid``` lancer la commande suivante

```bash
/usr/local/rancid/bin/rancid-cvs
```

Elle va permettre de créer les répertoires en fonction des groupes créés dans ```/usr/local/rancid/etc/rancid.conf```

```bash
[rancid@centos var]$ ll /usr/local/rancid/var/
total 0
drwxr-x---. 4 rancid netadm 106 26 déc 10:32 cisco-rt		# En fonction du rancid.conf
drwxr-x---. 4 rancid netadm 106 26 déc 10:30 cisco-sw		# En fonction du rancid.conf
drwxrwxr-x. 6 rancid netadm  92 26 déc 10:32 CVS
drwxr-x---. 2 rancid netadm   6 26 déc 10:30 logs
```

Ajouter les hosts dans les différents groupes.
Pour rendre le fichier plus clair, nous allons utiliser les noms DNS. Il faut donc faire le nécessaire dans le fichier ```/etc/hosts```

```bash
[root@centos etc]# vim /etc/hosts

#
#
# Manual ADD 
#
######################################################
## Routers
10.0.3.101      dh-01-rt-001
10.0.3.102      dh-01-rt-002

######################################################
## Switches
10.0.3.201      dh-01-sw-001
10.0.3.202      dh-01-sw-002
10.0.3.203		dh-01-sw-003
```

```bash
[rancid@centos etc]$ ping dh-01-rt-001
PING dh-01-rt-001 (10.0.3.101) 56(84) bytes of data.
64 bytes from dh-01-rt-001 (10.0.3.101): icmp_seq=1 ttl=255 time=19.7 ms
64 bytes from dh-01-rt-001 (10.0.3.101): icmp_seq=2 ttl=255 time=12.4 ms
```

Ajouter les équipements aux différents groupes.

```bash
[rancid@centos cisco-rt]$ pwd
/usr/local/rancid/var/cisco-rt
[rancid@centos cisco-rt]$ vim router.db
```

```bash
dh-01-rt-001;cisco;up
dh-01-rt-002;cisco;up
```

Il faut maintenant configurer le ```/home/rancid/.cloginrc```. Il faut décrire les paramètre pour se connecter aux équipements. Dans notre cas, c'est assez facile car tous les équipements on les identifiants ``admin/cisco``

```bash
add autoenable 	* 1
add method 		* ssh
add user 		* admin
add password 	* cisco
```

La configuration peut être beaucoup plus avancé et chaque device peut avoir ses propres méthodes de connexion ainsi que ses propres credentials

* Configuration par groupe
* Configuration avec une ```regex```
* Etc.

```bash
add autoenable 	dh-01-rt-001
add method 		dh-01-rt-001 ssh
add user 		dh-01-rt-001 dylan
add password 	dh-01-rt-001 dylanpassword
```

Tester la connexion aux équipements.

```bash
[rancid@centos ~]$ /usr/local/rancid/bin/clogin dh-01-rt-001
dh-01-rt-001
spawn ssh -c 3des -x -l admin dh-01-rt-001
The authenticity of host 'dh-01-rt-001 (10.0.3.101)' can't be established.
RSA key fingerprint is SHA256:MvnE9NbWD1dTtHhsGSf4lngq1F7Q8AJdIdsc6LxDP+Y.
RSA key fingerprint is MD5:8c:d6:2f:79:f8:f0:f9:d5:b8:3a:92:18:34:95:95:1d.
Are you sure you want to continue connecting (yes/no)? 
Host dh-01-rt-001 added to the list of known hosts.
yes
Warning: Permanently added 'dh-01-rt-001,10.0.3.101' (RSA) to the list of known hosts.
Password: 

dh-01-rt-001#
```

#### Petit problème rencontré sur GNS3

Il est intéressant en cas de problème de tester sa connexion ```ssh``` sans utiliser le module

```bash
[rancid@localhost cisco-rt]$ ssh -l admin dh-01-rt-001
Password: 

dh-01-rt-001#
```

Les IOS des Cisco sont assez vieux et il peut y avoir une problème avec les paramètre ```ssh```

```bash
➜  var clogin dh-01-rt-001
dh-01-rt-001
spawn ssh -x -l admin dh-01-rt-001
Unable to negotiate with 10.0.3.101 port 22: no matching key exchange method found. Their offer: diffie-hellman-group1-sha1
```

```bash
➜  var clogin dh-01-rt-001
dh-01-rt-001
spawn ssh -x -l admin dh-01-rt-001
Unable to negotiate with 10.0.3.101 port 22: no matching cipher found. Their offer: aes128-cbc,3des-cbc,aes192-cbc,aes256-cbc
```

Pour fixer ces problèmes, il suffit d'ajouter temporairement les lignes suivantes dans le fichier ```/etc/ssh/ssh_config```

```bash
Host *
        SendEnv LANG LC_*
        GSSAPIAuthentication yes
        KexAlgorithms +diffie-hellman-group1-sha1
        Ciphers aes128-ctr,aes192-ctr,aes256-ctr,aes128-cbc,3des-cbc
```

=> il est également possible d'ajouter tous ces paramètre dans le fichier ```/home/rancid/.cloginrc```



### ViewVC

http://www.viewvc.org/downloads/

```bash
cd /home/rancid/TAR
wget http://www.viewvc.org/downloads/viewvc-1.1.26.tar.gz
tar -xvzf viewvc-1.1.26.tar.gz
rm -rf viewvc-1.1.26.tar.gz 
cd viewvc-1.1.26
sudo ./viewvc-install
```

Ajouter les lignes suivantes

```bash
vim /usr/local/viewvc-1.1.26/viewvc.conf
```

```bash
# ~ ligne 54
root_parents = /usr/local/rancid/var/CVS:cvs

# ~ ligne 302
rcs_dir = /usr/local/bin

# ~ ligne 
use_rcsparse = 1
```



Configurer la partie ```httpd```

```bash
cp /usr/local/viewvc-1.1.26/bin/cgi/*.cgi /var/www/cgi-bin
chmod +x /var/www/cgi-bin/*.cgi
chown apache:apache /var/www/cgi-bin/*.cgi

vim /etc/httpd/conf/httpd.conf
```

```bash
# Custom Rancid Config

<VirtualHost *:80>
        DocumentRoot /var/www
        ScriptAlias /cgi-bin/ "/var/www/cgi-bin"
        ScriptAlias /viewvc /var/www/cgi-bin/viewvc.cgi
        ScriptAlias /query /var/www/cgi-bin/query.cgi
<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Order allow,deny
    Allow from all
</Directory>
</VirtualHost>

```



### MariaDB

```bash
yum install mariadb -y
```

```bash
systemctl enable mariadb
systemctl start mariadb
sudo mysql_secure_installation
```

* root password = root

Se conncter à MariaDB

```bash
mysql -u root -p
```

```mysql
CREATE USER 'VIEWVC'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'VIEWVC'@'localhost' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'VIEWVC'@'localhost' IDENTIFIED BY 'password';
FLUSH PRIVILEGES;
QUIT
```

Dans le ```tar``` téléchargé, il y a un scritp de création de la base de données.

```bash
cd /usr/local/viewvc-1.1.26/bin
```

```bash
[root@centos bin]# ./make-database
MySQL Hostname (leave blank for default): 
MySQL Port (leave blank for default): 
MySQL User: root
MySQL Password: root
ViewVC Database Name [default: ViewVC]: VIEWVC

Database created successfully.  Don't forget to configure the 
```

Créer un nouvel utilisateur

```mysql
CREATE USER VIEWVCRO@LOCALHOST;
GRANT ALL PRIVILEGES ON VIEWVC.* TO 'VIEWVCRO'@'localhost' WITH GRANT OPTION ;
GRANT ALL PRIVILEGES ON VIEWVC.* TO 'VIEWVCRO'@'localhost' IDENTIFIED BY 'password';
```

Modifier maintenant le fichier de configuration de ViewVC pour le connecter à la Database.

```bash
vim /usr/local/view-1.1.26/viewvc.conf
```

```bash
[cvsdb]

## enabled: Enable database integration feature.
##
enabled = 1

## host: Database hostname.  Leave unset to use a local Unix socket
## connection.
##
host = localhost

## post: Database listening port.
##
port = 3306

## database_name: ViewVC database name.
##
database_name = VIEWVC

## user: Username of user with read/write privileges to the database
## specified by the 'database_name' configuration option.
##
user = VIEWVC

## passwd: Password of user with read/write privileges to the database
## specified by the 'database_name' configuration option.
##
passwd = password

## readonly_user: Username of user with read privileges to the database
## specified by the 'database_name' configuration option.
##
readonly_user = VIEWVCRO 

## readonly_passwd: Password of user with read privileges to the database
## specified by the 'database_name' configuration option.
##
readonly_passwd = password 
```

```bash
/usr/local/viewvc-1.1.26/bin/cvsdbadmin rebuild /usr/local/rancid/var/CVS/CVSROOT/
```



Se connecter en http sur l'adresse IP de la machine

http:// {{ IP_DE_LA_MACHINE }} /viewvc

![viewvc_home](./viewvc_home.png)

#### Problème ?

Il se peut que le Firewall local de la machine bloque les requêtes HTTP/S

```bash
firewall-cmd --permanent --zone=public --add-service=http
firewall-cmd --permanent --zone=public --add-service=https
firewall-cmd --reload
```



On peut maintenant relancer ```/usr/bin/rancid/bin/rancid-run cisco-rt``` pour obternir un backup. On pourra ainsi voir la différence entre les deux fichiers.

```cisco-rt``` ==> ```configs/``` ==> ```dh-01-rt-001``` 

![viewvc_rt](./viewvc_rt.png)

Cliquer maintenant sur ```Diff to previous 1.1```. On verra les lignes qui ont été ajoutées et supprimées dans la configuration du du serveur.

![viewvc_diff](./viewvc_diff.png)



### Problème - groupe absent dans ```/usr/local/rancid/var/CVS```

Si vos dossier ne se retrouve pas pas dans le Dossier. Exemple :

```bash
[rancid@localhost var]$ ls
cisco-rt  cisco-sw  CVS  logs

[rancid@localhost var]$ ls CVS/
CVSROOT
```

Supprimer les deux dossiers

```bash
[rancid@localhost var]$ rm -rf cisco-sw/
[rancid@localhost var]$ rm -rf cisco-rt/
```

Relancer la commande ```rancid-cvs```

```bash
[rancid@localhost var]$ /usr/local/rancid/bin/rancid-cvs 

No conflicts created by this import

cvs checkout: Updating cisco-rt
Directory /usr/local/rancid/var/CVS/cisco-rt/configs added to the repository
cvs commit: Examining configs
cvs add: scheduling file `router.db' for addition
cvs add: use 'cvs commit' to add this file permanently
RCS file: /usr/local/rancid/var/CVS/cisco-rt/router.db,v
done
Checking in router.db;
/usr/local/rancid/var/CVS/cisco-rt/router.db,v  <--  router.db
initial revision: 1.1
done

No conflicts created by this import

cvs checkout: Updating cisco-sw
Directory /usr/local/rancid/var/CVS/cisco-sw/configs added to the repository
cvs commit: Examining configs
cvs add: scheduling file `router.db' for addition
cvs add: use 'cvs commit' to add this file permanently
RCS file: /usr/local/rancid/var/CVS/cisco-sw/router.db,v
done
Checking in router.db;
/usr/local/rancid/var/CVS/cisco-sw/router.db,v  <--  router.db
initial revision: 1.1
done
```

Il faudra également relancer les deux commande ci-dessous et remplir les fichier ```/usr/local/rancid/var/cisco-rt/router.db```

```bash
[rancid@localhost logs]$ /usr/local/viewvc-1.1.26/bin/cvsdbadmin rebuild /usr/local/rancid/var/CVS/
Using repository root `/usr/local/rancid/var/CVS'
Purging existing data for repository root `/usr/local/rancid/var/CVS'
[CVSROOT/loginfo [0 commits]]
[CVSROOT/rcsinfo [0 commits]]
[CVSROOT/editinfo [0 commits]]
[CVSROOT/verifymsg [0 commits]]
[CVSROOT/commitinfo [0 commits]]
[CVSROOT/taginfo [0 commits]]
[CVSROOT/checkoutlist [0 commits]]
[CVSROOT/cvswrappers [0 commits]]
[CVSROOT/notify [0 commits]]
[CVSROOT/modules [0 commits]]
[CVSROOT/config [0 commits]]
[cisco-rt/configs/.cvsignore [0 commits]]
[cisco-rt/router.db [0 commits]]
[cisco-rt/.cvsignore [0 commits]]
[cisco-sw/configs/.cvsignore [0 commits]]
[cisco-sw/router.db [0 commits]]
[cisco-sw/.cvsignore [0 commits]]

```

Remplir le fichier

```
[rancid@localhost logs]$ /usr/local/rancid/bin/rancid-run
```



### SVN

compatiable avec SVN

### Gitlab

compatible avec GIT.

*<u>**En cours.**</u>*



Monter un container Gitlab
https://hub.docker.com/r/gitlab/gitlab-ce/dockerfile
https://docs.gitlab.com/omnibus/docker/

```Dockerfile
FROM gitlab/gitlab-ce

# Install SNMP Observium
RUN apt-get update -y
RUN apt-get install -y snmp snmp-mibs-downloader

VOLUME ./data:/var/opt/gitlab
VOLUME ./logs:/var/logs/gitlab
VOLUME ./config:/etc/gitlab

EXPOSE 22443:443
EXPOSE 2280:80
EXPOSE 2222:22
```

```bash
docker build -t dylanhamel/gitlab-ce .
```

```bash
docker run -d -p 2280:80 -p 22443:443 -p 2222:22 --name gitlab --restart always dylanhamel/gitlab-ce
```

![gitlab_start](./gitlab_start.png)Il faut 2-3 minutes avant de pouvoir joindre le Container.
Il faudra également mettre un mot de passe pour le compte root

![gitlab_dashboard](./gitlab_dashboard.png)



## Sources

* https://www.petenetlive.com/KB/Article/0001331

* https://nsrc.org/workshops/2015/wacren-nmm/raw-attachment/wiki/Agenda/rancid-exercise.htm

* https://layer77.net/2016/08/10/upgrading-from-rancid-2-3-8-to-3-4-1/

* http://www.shrubbery.net/rancid/

* [http://www.linuxhomenetworking.com/wiki/index.php/Quick_HOWTO_:_Ch1_:_Network_Backups_With_Rancid#.XCOIes9KjOQ](http://www.linuxhomenetworking.com/wiki/index.php/Quick_HOWTO_:_Ch1_:_Network_Backups_With_Rancid#.XCOIes9KjOQ)

* 
